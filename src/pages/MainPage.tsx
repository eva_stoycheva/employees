import React from "react";
import Router from "../components/Router";

const MainPage: React.FC<{}> = ({}) => {
  return <Router />;
};

export default MainPage;
