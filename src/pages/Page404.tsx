import { Container } from "@mui/material";
import React from "react";

const Page404: React.FC<{}> = () => {
  return (
    <Container>
      <img
        src="https://static.vecteezy.com/system/resources/previews/002/416/534/original/404-error-and-page-not-found-illustration-vector.jpg"
        alt="Page not found! :("
        width="100%"
        height="100%"
      />
    </Container>
  );
};

export default Page404;
