import { EmployeeType } from "../model/EmployeeType";

export const DEPARTMENT = "department";

export const employeesData: EmployeeType[] = [
  {
    id: 1,
    name: "Tiger Nixon",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Human Resources",
  },
  {
    id: 2,
    name: "Garrett Winters",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "R & D",
  },
  {
    id: 3,
    name: "Ashton Cox",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "R & D",
  },
  {
    id: 4,
    name: "Cedric Kelly",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "R & D",
  },
  {
    id: 5,
    name: "Airi Satou",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "R & D",
  },
  {
    id: 6,
    name: "Brielle Williamson",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 7,
    name: "Herrod Chandler",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 8,
    name: "Rhona Davidson",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 9,
    name: "Colleen Hurst",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 10,
    name: "Sonya Frost",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 11,
    name: "Jena Gaines",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 12,
    name: "Quinn Flynn",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Management",
  },
  {
    id: 13,
    name: "Charde Marshall",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Accounting",
  },
  {
    id: 14,
    name: "Haley Kennedy",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Accounting",
  },
  {
    id: 15,
    name: "Tatyana Fitzpatrick",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Accounting",
  },
  {
    id: 16,
    name: "Michael Silva",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Accounting",
  },
  {
    id: 17,
    name: "Paul Byrd",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 18,
    name: "Gloria Little",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 19,
    name: "Bradley Greer",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 20,
    name: "Dai Rios",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 21,
    name: "Jenette Caldwell",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 22,
    name: "Yuri Berry",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 23,
    name: "Caesar Vance",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
  {
    id: 24,
    name: "Doris Wilder",
    image: "https://dl.memuplay.com/new_market/img/com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png",
    department: "Engineering",
  },
];

export const departmentsData = [
  {
    id: 1,
    name: "Engineering",
  },
  {
    id: 2,
    name: "Accounting",
  },
  {
    id: 3,
    name: "Management",
  },
  {
    id: 4,
    name: "R & D",
  },
  {
    id: 5,
    name: "Human Resources",
  },
];
