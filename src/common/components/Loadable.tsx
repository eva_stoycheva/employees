import { LinearProgress } from "@mui/material";
import React, { Suspense } from "react";

export const Loadable = (Component: React.FC) => (props: any) => {
  return (
    <Suspense fallback={<LinearProgress />}>
      <Component {...props} />
    </Suspense>
  );
};
