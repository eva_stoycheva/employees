import { chain } from "lodash";
import { DEPARTMENT, departmentsData, employeesData } from "./constants";
import { EmployeeType } from "../model/EmployeeType";

export const groupByDepartments = (employees: EmployeeType[]) =>
  chain(employees)
    .groupBy(DEPARTMENT)
    .map((value, key) => ({ department: key, employees: value }))
    .value();

export const getEmployeesByDepartment = (employees: EmployeeType[], filter: string[]) => {
  return employees.filter(e => filter.includes(e.department));
};

export const departments = departmentsData.filter(d => d.name).map(d => d.name);

export const employeesNames = employeesData.filter(e => e.name).map(e => e.name);

export const filterByName = (employees: EmployeeType[], selectedNames: string | string[]) => {
  return employees.filter(employee => employee.name === selectedNames);
};
