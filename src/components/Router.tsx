import React, { lazy } from "react";
import { Navigate, useRoutes } from "react-router-dom";
import { Loadable } from "../common/components/Loadable";
import Routes from "../model/routes";

const Page404 = Loadable(lazy(() => import("../pages/Page404")));
const Dashboard = Loadable(lazy(() => import("../components/Dashboard")));

const Router: React.FC<{}> = () => {
  return useRoutes([
    {
      path: Routes.ROOT,
      element: <Navigate to={Routes.DASHBOARD} />,
    },
    {
      path: Routes.DASHBOARD,
      element: <Dashboard />,
    },
    {
      path: Routes.NOT_FOUND,
      element: <Page404 />,
    },
    {
      path: "*",
      element: <Navigate to={Routes.NOT_FOUND} replace />,
    },
  ]);
};

export default Router;
