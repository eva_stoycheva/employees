import { FormControlLabel, FormGroup, Switch } from "@mui/material";
import React from "react";
import { Mode, ThemeModeType } from "../model/ThemeModeType";

const ThemeMode: React.FC<ThemeModeType> = ({ mode, setMode }) => {
  const toggleMode = () => setMode(mode === Mode.DARK ? Mode.LIGHT : Mode.DARK);

  return (
    <FormGroup sx={{ mt: 1, px: { xs: 2, sm: 2, md: 8, lg: 8 } }}>
      <FormControlLabel control={<Switch onChange={toggleMode} />} label="Dark Theme" />
    </FormGroup>
  );
};

export default ThemeMode;
