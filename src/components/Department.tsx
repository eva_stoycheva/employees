import { Grid, Paper, Typography } from "@mui/material";
import React from "react";
import { DepartmentType } from "../model/DepartmentType";

export const Department: React.FC<DepartmentType> = ({ department, children }) => {
  return (
    <>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Paper>
          <Typography align="center" variant="h5">
            {department}
          </Typography>
        </Paper>
      </Grid>
      {children}
    </>
  );
};
