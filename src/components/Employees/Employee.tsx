import { Card, CardContent, CardMedia, Checkbox, Divider, Grid, Stack, Typography } from "@mui/material";
import React, { useState } from "react";

interface Props {
  name: string
  image: string
  selectedAll?: boolean
}

export const Employee: React.FC<Props> = ({ name, image, selectedAll }) => {
  const [selectedEmployee, setSelectedEmployee] = useState<boolean>(false);

  const handleChecked = () => setSelectedEmployee(!selectedEmployee);
  const isChecked = () => (selectedAll ? selectedAll : selectedEmployee);

  return (
    <Grid item sm={6} md={4} lg={2}>
      <Card raised>
        <CardMedia component="img" image={image} alt="avatar" />
        <Divider variant="middle" />
        <CardContent>
          <Stack direction="row" sx={{ alignItems: "center", justifyContent: "center" }}>
            <Checkbox size="medium" checked={isChecked()} onClick={handleChecked} />
            <Typography variant="body1">{name}</Typography>
          </Stack>
        </CardContent>
      </Card>
    </Grid>
  );
};
