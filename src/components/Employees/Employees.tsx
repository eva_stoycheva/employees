import { Grid } from "@mui/material";
import React, { useMemo } from "react";
import { employeesData } from "../../common/constants";
import { FIltersType } from "../../model/FiltersType";
import { Department } from "../Department";
import { getEmployeesByDepartment, groupByDepartments } from "../../common/utils";
import { Employee } from "./Employee";

export const Employees: React.FC<FIltersType> = ({ filter, selectedAll, employees }) => {
  const filteredEmployees = useMemo(() => getEmployeesByDepartment(employees, filter), [employees, filter]);
  const groupedByDepartment = useMemo(() => groupByDepartments(filteredEmployees), [filteredEmployees]);

  return (
    <Grid container spacing={3}>
      {filter.length && employeesData.length
        ? groupedByDepartment.map(({ department, employees }, index: number) => {
          return (
            <>
              <Department department={department} key={`${department}_${index}`} />
              {employees.map(e => {
                return (
                  <Employee
                    key={`${e.name}_${e.id}`}
                    name={e.name}
                    image={e.image}
                    selectedAll={selectedAll}
                  />
                );
              })}
            </>
          );
        })
        : employees.map(({ id, name, image, department }) => {
          return <Employee key={id} name={name} image={image} selectedAll={selectedAll} />;
        })}
    </Grid>
  );
};
