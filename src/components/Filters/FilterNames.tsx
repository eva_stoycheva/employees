import ClearIcon from "@mui/icons-material/Clear";
import {
  Box,
  Chip,
  FormControl,
  IconButton,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import React, { useState } from "react";
import { employeesData } from "../../common/constants";
import { employeesNames, filterByName } from "../../common/utils";
import { EmployeeType } from "../../model/EmployeeType";
import { FilterNamesType } from "../../model/FilterNameType";
import { MenuProps } from "../../styles/MenuPropsStyle";

export const FilterNames: React.FC<FilterNamesType> = ({ employees, setEmployees }) => {
  const [selectedName, setSelectedName] = useState<string>("");

  const handleSelectedNames = (event: SelectChangeEvent<typeof selectedName>) => {
    const {
      target: { value },
    } = event;
    const filtered = filterByName(employees, value);
    setSelectedName(value);
    setEmployees && setEmployees(filtered);
  };

  const handleClearClick = () => {
    setSelectedName("");
    setEmployees && setEmployees(employeesData);
  };

  return (
    <FormControl fullWidth>
      <InputLabel id="departments">Employee names</InputLabel>
      <Select
        labelId="departments"
        id="departments-checkbox"
        value={selectedName}
        onChange={handleSelectedNames}
        input={<OutlinedInput label="Departments" />}
        renderValue={selected => (
          <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
            <Chip key={selected} label={selected} />
          </Box>
        )}
        endAdornment={
          <IconButton sx={{ visibility: selectedName.length ? "visible" : "hidden" }} onClick={handleClearClick}>
            <ClearIcon />
          </IconButton>
        }
        MenuProps={MenuProps}
      >
        {employeesNames.map((name: string, index: number) => (
          <MenuItem key={`${name}_${index}`} value={name} divider>
            <ListItemText primary={name} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
