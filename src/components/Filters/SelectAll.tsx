import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { SelectedAllType } from "../../model/SelectedAllType";

export const SelectAll: React.FC<SelectedAllType> = ({ selectedAll, setSelectedAll }) => {
  const handleSelectedAll = () => {
    setSelectedAll && setSelectedAll(!selectedAll);
  };

  return (
    <FormControlLabel
      control={<Checkbox size="medium" checked={selectedAll} onClick={handleSelectedAll} />}
      label="Select All"
    />
  );
};
