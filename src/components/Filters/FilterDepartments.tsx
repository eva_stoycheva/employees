import ClearIcon from "@mui/icons-material/Clear";
import {
  Chip,
  FormControl,
  IconButton,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { FilterDepartmentType } from "../../model/FilterDepartmentType";
import { MenuProps } from "../../styles/MenuPropsStyle";
import { departments } from "../../common/utils";

export const FilterDepartments: React.FC<FilterDepartmentType> = ({ filter, setFilter }) => {
  const handleChange = (event: SelectChangeEvent<typeof filter>) => {
    const {
      target: { value },
    } = event;
    setFilter && setFilter(typeof value === "string" ? value.split(",") : value);
  };
  const handleClearClick = () => {
    setFilter && setFilter([]);
  };

  return (
    <>
      <FormControl fullWidth>
        <InputLabel id="departments">Departments</InputLabel>
        <Select
          labelId="departments"
          id="departments-checkbox"
          value={filter}
          multiple
          onChange={handleChange}
          input={<OutlinedInput label="Departments" />}
          renderValue={selected => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {selected.map(value => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          endAdornment={
            <IconButton sx={{ visibility: filter.length ? "visible" : "hidden" }} onClick={handleClearClick}>
              <ClearIcon />
            </IconButton>
          }
          MenuProps={MenuProps}
        >
          {departments.map((name: string) => (
            <MenuItem key={name} value={name} divider>
              <ListItemText primary={name} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
};
