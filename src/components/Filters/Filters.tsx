import { Grid } from "@mui/material";
import React from "react";
import { FIltersType } from "../../model/FiltersType";
import { FilterDepartments } from "./FilterDepartments";
import { FilterNames } from "./FilterNames";
import { SelectAll } from "./SelectAll";

export const Filters: React.FC<FIltersType> = ({
  filter,
  setFilter,
  selectedAll,
  setSelectedAll,
  employees,
  setEmployees,
}) => {
  return (
    <Grid container spacing={3} alignItems="center" justifyContent="space-between">
      <Grid item xs={12} sm={12} md={2} lg={2}>
        <SelectAll selectedAll={selectedAll} setSelectedAll={setSelectedAll} />
      </Grid>
      <Grid item xs={12} sm={12} md={5} lg={5}>
        <FilterDepartments filter={filter} setFilter={setFilter} />
      </Grid>
      <Grid item xs={12} sm={12} md={5} lg={5}>
        <FilterNames employees={employees} setEmployees={setEmployees} />
      </Grid>
    </Grid>
  );
};
