import { Grid } from "@mui/material";
import React, { useState } from "react";
import { employeesData } from "../common/constants";
import { EmployeeType } from "../model/EmployeeType";
import { Employees } from "./Employees/Employees";
import { Filters } from "./Filters/Filters";

const Dashboard: React.FC<{}> = () => {
  const [filter, setFilter] = useState<string[]>([]);
  const [selectedAll, setSelectedAll] = useState<boolean>(false);
  const [employees, setEmployees] = useState<EmployeeType[]>(employeesData);

  return (
    <Grid container spacing={2} px={{ xs: 2, sm: 2, md: 8, lg: 8 }} justifyContent="space-between">
      <Grid item sm={12}>
        <Filters
          filter={filter}
          setFilter={setFilter}
          selectedAll={selectedAll}
          setSelectedAll={setSelectedAll}
          employees={employees}
          setEmployees={setEmployees}
        />
      </Grid>
      <Grid item sm={12}>
        <Employees filter={filter} selectedAll={selectedAll} employees={employees} />
      </Grid>
    </Grid>
  );
};

export default Dashboard;
