import { CssBaseline, PaletteMode, ThemeProvider } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import MainPage from "./pages/MainPage";
import React, { useMemo, useState } from "react";
import { Mode } from "./model/ThemeModeType";
import ThemeMode from "./components/ThemeMode";
import { getTheme } from "./styles/theme";

function App() {
  const [mode, setMode] = useState<PaletteMode>(Mode.LIGHT);
  const theme = useMemo(() => getTheme(mode), [mode]);

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <ThemeMode mode={mode} setMode={setMode} />
        <MainPage />
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
