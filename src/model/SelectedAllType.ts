export interface SelectedAllType {
  selectedAll?: boolean;
  setSelectedAll?: (selectedAll: boolean) => void;
}
