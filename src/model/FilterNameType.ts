import { EmployeeType } from "./EmployeeType";

export interface FilterNamesType {
  employees: EmployeeType[];
  setEmployees?: (employees: EmployeeType[]) => void;
}
