export interface FilterDepartmentType {
  filter: string[];
  setFilter?: (filter: string[]) => void;
}
