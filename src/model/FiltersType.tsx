import { FilterDepartmentType } from "./FilterDepartmentType";
import { FilterNamesType } from "./FilterNameType";
import { SelectedAllType } from "./SelectedAllType";

export type FIltersType = FilterDepartmentType & SelectedAllType & FilterNamesType;
