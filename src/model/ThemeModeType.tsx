export interface ThemeModeType {
  mode: ThemeMode;
  setMode: (mode: ThemeMode) => void;
}

export type ThemeMode = "light" | "dark";

export enum Mode {
  LIGHT = "light",
  DARK = "dark",
}
