import { BaseEntityType } from "./BaseEntityType";
import { ReactNode } from "react";

export interface DepartmentType extends BaseEntityType {
  department?: string;
  children?: ReactNode;
}
