import { BaseEntityType } from "./BaseEntityType";

export interface EmployeeType extends BaseEntityType {
  name: string;
  image: string;
  department: string;
  select?: string;
}
