enum Routes {
  DASHBOARD = "/dashboard",
  NOT_FOUND = "/404",
  ROOT = "/",
}

export default Routes;
