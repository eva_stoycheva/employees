import { createTheme } from "@mui/material/styles";
import { Mode, ThemeMode } from "../model/ThemeModeType";
import { darkPalette, lightPalette } from "./palette";
import { typography } from "./typography";

export const getTheme = (mode: ThemeMode) => {
  return createTheme({ palette: mode === Mode.DARK ? darkPalette : lightPalette, typography: typography });
};
