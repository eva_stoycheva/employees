export const typography = {
  fontFamily: ["-apple-system"].join(","),
  subtitle1: {
    fontSize: 12,
  },
};
