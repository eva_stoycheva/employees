import { Mode } from "../model/ThemeModeType";

export const darkPalette = {
  mode: Mode.DARK,
  background: {
    default: "#16113A",
    paper: "#272953",
  },
};

export const lightPalette = {
  mode: Mode.LIGHT,
  primary: {
    main: "#272953",
  },
  background: {
    default: "#fafafa",
    paper: "#ede7f6",
  },
};
